//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Jossué Dután on 9/5/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    var itemManager:ItemManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true) //Salir al otro View Controller con pop
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = titleTextField.text ?? "" //quitando la opcionalidad, si falla se llena con vacios
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextField.text ?? ""
        
        /*if titleTextField.text!.isEmpty || descriptionTextField.text!.isEmpty {
            print("No dejar vacio titulo y descripcion")
        }*/
        
        /*
        let item = Item(
            id: UUID(),
            title: itemTitle,
            location: itemLocation,
            description: itemDescription
        )*/
        
        if itemTitle == "" {
            showAlert(title: "Error", message: "Title is required")
        }
        
        //itemManager!.toDoItems += [item]
        itemManager?.addItem(title: itemTitle, location: itemLocation, itemDescription: itemDescription)
 
        navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String){
        let alert = UIAlertController(title:title, message:message, preferredStyle: .alert) //.actionSheet
        let okAction = UIAlertAction (title: "OK", style: .default, handler: nil)
        //handler permite ejecutar código
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
