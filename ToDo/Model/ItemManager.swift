//
//  ItemManager.swift
//  ToDo
//
//  Created by Jossué Dután on 9/5/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import Foundation
import RealmSwift

class ItemManager {
    var toDoItems:[Item] = []
    var doneItems:[Item] = []
    var realm:Realm //una instancia que se está compartiendo durante toda la aplicación
    
    init(){
        realm = try! Realm()
        print(realm.configuration.fileURL) 
    }
    
    func checkItem(index:Int){
        //let item  = toDoItems.remove(at: index)
        //doneItems += [item]
        
        let item = toDoItems[index]
        try! realm.write {
            item.done = true
        }
        
        
    }
    
    func unCheckItem(index:Int){
        //let item = doneItems.remove(at: index)
        //toDoItems += [item]
        let item = doneItems[index]
        try! realm.write {
            item.done = false
        }
    }
    
    func addItem(title:String, location:String, itemDescription:String?){
        let item = Item()
        item.id = "\(UUID())"
        item.title = title
        item.location = location
        item.itemDescription = itemDescription
        
        try! realm.write {
            realm.add(item)
        }
    }
    
    func updateArrays(){
        toDoItems = Array(realm.objects(Item.self).filter("done = false"))
        doneItems = Array(realm.objects(Item.self).filter("done = true"))
    }
    
}
