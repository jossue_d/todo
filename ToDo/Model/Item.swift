//
//  Item.swift
//  ToDo
//
//  Created by Jossué Dután on 9/5/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    @objc dynamic var id:String?
    @objc dynamic var title:String?
    @objc dynamic var location: String?
    @objc dynamic var itemDescription:String?
    @objc dynamic var done = false //si se agrega un nuevo atributo sale un error de Migration is required
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
/*
struct Item {
    let title:String
    let location:String
    let description:String
}
*/
