//
//  ViewController.swift
//  ToDo
//
//  Created by Jossué Dután on 8/5/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let itemManager = ItemManager()
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItems.count
        }
        return itemManager.doneItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell()
        //cell.textLabel?.text = "\(indexPath)"
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell") as! ItemTableViewCell
        if indexPath.section == 0 {
            cell.titleLabel.text = itemManager.toDoItems[indexPath.row].title
            cell.locationLabel.text = itemManager.toDoItems[indexPath.row].location
        } else {
            cell.titleLabel.text = itemManager.doneItems[indexPath.row].title
            cell.locationLabel.text = ""
        }
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*let item1 = Item (title: "toDo1", location: "Office", description: "Do something")
        let item2 = Item (title: "toDo2", location: "House", description: "Do anything")
        let item3 = Item (title: "toDo3", location: "College", description: "Do tasks")
        
        itemManager.toDoItems = [item1,item2, item3]
        
        let item4 = Item (title: "done1", location: "Uni", description: "iOS")
        
        itemManager.doneItems = [item4]*/
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddItemSegue"{
            let destination = segue.destination as! AddItemViewController
            destination.itemManager = itemManager
        }
        if segue.identifier == "toItemInfoSegue"{
            let destination = segue.destination as! ItemInfoViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "toItemInfoSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To Do" : "Done"
    }
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return indexPath.section == 0 ? "Check" : "UnCheck"
    }
    func empty(){
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            itemManager.checkItem(index: indexPath.row)
        }
        else {
            itemManager.unCheckItem(index: indexPath.row)
        }
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }
}

